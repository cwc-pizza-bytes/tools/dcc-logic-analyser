#include "DCCAnalyzerResults.h"
#include <AnalyzerHelpers.h>
#include "DCCAnalyzer.h"
#include "DCCAnalyzerSettings.h"
#include <iostream>
#include <fstream>

DCCAnalyzerResults::DCCAnalyzerResults( DCCAnalyzer* analyzer, DCCAnalyzerSettings* settings )
    : AnalyzerResults(), mSettings( settings ), mAnalyzer( analyzer )
{
}

DCCAnalyzerResults::~DCCAnalyzerResults()
{
}

void DCCAnalyzerResults::GenerateBubbleText( U64 frame_index, Channel& channel, DisplayBase display_base )
{
    ClearResultStrings();
    Frame frame = GetFrame( frame_index );

    switch( frame.mData2 )
    {
    case FT_Preamble:
    {
        char number_str[ 128 ];
        AnalyzerHelpers::GetNumberString( frame.mData1 & 0xFFFF, Decimal, 16, number_str, 128 );
        AddResultString( "P" );
        AddResultString( "Pre" );
        AddResultString( "Preamble" );
        AddResultString( "Preamble (", number_str, ")" );
        if( frame.mFlags )
            AddResultString( "Preamble (", number_str, ") too few" );
        break;
    }

    case FT_Data:
    {
        char number_str[ 128 ];
        AnalyzerHelpers::GetNumberString( frame.mData1 & 0x7F, display_base, 8, number_str, 128 );
        // Each packet is 4 long, so the interesting bits of data are only 1,2 + 5,6 etc
        // I can find this using (x % 4) -1 == 0, and (x % 4) -2 == 0;
        const auto is_address = ( frame_index % 4 ) - 1 == 0;
        const auto is_instruction = ( frame_index % 4 ) - 2 == 0;

        // AddResultString( "D" );
        std::cout << "frame_index = " << std::dec << frame_index << " is an address " << is_address << "\n";
        std::cout << "frame_index = " << std::dec << frame_index << " is an instruction " << is_instruction << "\n";
        if( is_address )
        {
            AddResultString( "Address (", number_str, ")" );
            AddResultString( "Addr(", number_str, ")" );
            AddResultString( "A(", number_str, ")" );
            AddResultString( "A" );
        }
        else if( is_instruction )
        {
            const auto instruction_type = frame.mData1 & 0b11000000;
            if( instruction_type == 0b01000000 )
            {
                const auto data = frame.mData1 & 0b00111111;
                const auto direction_bit = data & 0b00100000;
                const auto speed_lsb = ( data & 0b00010000 ) >> 4;
                const auto speed = ( ( data & 0x0f ) << 1 ) | speed_lsb;
                std::cout << "data = " << number_str << ", lsb = " << speed_lsb << " speed = " << speed << "\n";
                AddResultString( "Instruction" );
                AddResultString( "I" );
                AddResultString( "Inst" );
                AddResultString( "Speed (", std::to_string( speed ).c_str(), ") ", direction_bit ? "Forward" : "Reverse" );
            }
        }
        break;
    }

    case FT_Checksum:
    {
        char number_str[ 128 ];
        AnalyzerHelpers::GetNumberString( frame.mData1 & 0xFF, display_base, 8, number_str, 128 );
        AddResultString( "C" );
        AddResultString( "Chk" );
        AddResultString( "Chk ", number_str );
        AddResultString( "Checksum ", number_str );
        if( ( frame.mFlags & DISPLAY_AS_ERROR_FLAG ) != 0 )
        {
            char expected_str[ 128 ];
            AnalyzerHelpers::GetNumberString( ( frame.mData1 >> 8 ) & 0xFF, display_base, 8, expected_str, 128 );
            AddResultString( "Checksum ", number_str, " expected ", expected_str );
        }
        break;
    }

    default:
    {
        AddResultString( "?" );
        AddResultString( "Analyzer error" );
        break;
    }
    }
}

void DCCAnalyzerResults::GenerateExportFile( const char* file, DisplayBase display_base, U32 export_type_user_id )
{
    std::ofstream file_stream( file, std::ios::out );

    U64 trigger_sample = mAnalyzer->GetTriggerSample();
    U32 sample_rate = mAnalyzer->GetSampleRate();

    file_stream << "Time [s],Value,Type" << std::endl;

    U64 num_frames = GetNumFrames();
    for( U32 i = 0; i < num_frames; i++ )
    {
        Frame frame = GetFrame( i );

        char time_str[ 128 ];
        AnalyzerHelpers::GetTimeString( frame.mStartingSampleInclusive, trigger_sample, sample_rate, time_str, 128 );

        char number_str[ 128 ];
        AnalyzerHelpers::GetNumberString( frame.mData1 & 0xFF, display_base, 8, number_str, 128 );

        file_stream << time_str << "," << number_str << ",";

        switch( frame.mData2 )
        {
        case FT_Preamble:
            file_stream << "Preamble" << std::endl;
            break;

        case FT_Data:
            file_stream << "Data" << std::endl;
            break;

        case FT_Checksum:
            file_stream << "Checksum" << std::endl;
            break;

        default:
            file_stream << "Analyzer error" << std::endl;
            break;
        }


        if( UpdateExportProgressAndCheckForCancel( i, num_frames ) == true )
        {
            file_stream.close();
            return;
        }
    }

    file_stream.close();
}

void DCCAnalyzerResults::GenerateFrameTabularText( U64 frame_index, DisplayBase display_base )
{
    /*
        Frame frame = GetFrame( frame_index );
        ClearTabularText();

        char number_str[128];
        AnalyzerHelpers::GetNumberString( frame.mData1, display_base, 8, number_str, 128 );
        AddTabularText( number_str );
    */

    ClearTabularText();
    Frame frame = GetFrame( frame_index );

    switch( frame.mData2 )
    {
    case FT_Preamble:
    {
        char number_str[ 128 ];
        AnalyzerHelpers::GetNumberString( frame.mData1 & 0xFFFF, Decimal, 16, number_str, 128 );
        if( frame.mFlags )
            AddTabularText( "Preamble (", number_str, ") too few" );
        else
            AddTabularText( "Preamble (", number_str, ")" );
        break;
    }

    case FT_Data:
    {
        char number_str[ 128 ];
        AnalyzerHelpers::GetNumberString( frame.mData1 & 0xFF, display_base, 8, number_str, 128 );
        AddTabularText( "Data ", number_str );
        break;
    }

    case FT_Checksum:
    {
        char number_str[ 128 ];
        AnalyzerHelpers::GetNumberString( frame.mData1 & 0xFF, display_base, 8, number_str, 128 );
        if( ( frame.mFlags & DISPLAY_AS_ERROR_FLAG ) != 0 )
        {
            char expected_str[ 128 ];
            AnalyzerHelpers::GetNumberString( ( frame.mData1 >> 8 ) & 0xFF, display_base, 8, expected_str, 128 );
            AddTabularText( "Checksum error ", number_str, " expected ", expected_str );
        }
        else
        {
            AddTabularText( "Checksum ", number_str );
        }
        break;
    }

    default:
    {
        AddTabularText( "Analyzer error" );
        break;
    }
    }
}

void DCCAnalyzerResults::GeneratePacketTabularText( U64 packet_id, DisplayBase display_base )
{
    ClearResultStrings();
    AddResultString( "not supported" );
}

void DCCAnalyzerResults::GenerateTransactionTabularText( U64 transaction_id, DisplayBase display_base )
{
    ClearResultStrings();
    AddResultString( "not supported" );
}